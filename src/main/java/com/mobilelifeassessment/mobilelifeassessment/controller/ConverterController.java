package com.mobilelifeassessment.mobilelifeassessment.controller;

import com.mobilelifeassessment.mobilelifeassessment.exception.ValidationException;
import com.mobilelifeassessment.mobilelifeassessment.request.ConvertRequest;
import com.mobilelifeassessment.mobilelifeassessment.service.ConverterService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/converter")
@RequiredArgsConstructor
public class ConverterController {
    private final ConverterService converterService;

    @PostMapping("/convert")
    @CircuitBreaker(name = "converter", fallbackMethod = "converterFallback")
    @TimeLimiter(name = "converter")
    @Retry(name = "converter")
    public ResponseEntity<Double> convert(@Valid @RequestBody ConvertRequest convertRequest) throws IOException {
        try {
            Double convertedValue = this.converterService.convert(convertRequest);
            return ResponseEntity.ok(convertedValue);
        } catch (Exception exception) {
            String message = exception.getMessage();
            Map<String, String> errors = new HashMap<>();
            if (message.equals("Currency not available")) {
                errors.put("to", "The currency " + convertRequest.getTo() + " does not exists to our service");
            } else if (message.equals("Only EUR is allowed as base currency")) {
                errors.put("to", message);
            }

            if (!errors.isEmpty()) {
                throw new ValidationException(errors);
            }

            throw exception;
        }
    }

    public CompletableFuture<String> converterFallback(ConvertRequest convertRequest, RuntimeException runtimeException) {
        return CompletableFuture.supplyAsync(
                () -> "An error occurred, please try again later!");
    }

}
