package com.mobilelifeassessment.mobilelifeassessment.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(
                title = "Converter API",
                description = "Converted application API endpoints"
        )
)
public class SwaggerConfig {
}
