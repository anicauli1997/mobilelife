package com.mobilelifeassessment.mobilelifeassessment.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationErrorResponse {
    private final Object errors;
    private final String message = "The given data is invalid";

    public ValidationErrorResponse(Object errors) {
        this.errors = errors;
    }

}
