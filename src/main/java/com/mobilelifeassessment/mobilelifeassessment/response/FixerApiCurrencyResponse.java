package com.mobilelifeassessment.mobilelifeassessment.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FixerApiCurrencyResponse {
    private Boolean success;
    private Integer timestamp;
    private String base;
    private String date;
    private Map<String, Double> rates;
}
