package com.mobilelifeassessment.mobilelifeassessment.command;

import com.mobilelifeassessment.mobilelifeassessment.service.ConverterService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ClearCurrencyFileRunner implements CommandLineRunner {

    @Autowired
    private ConverterService converterService;

    @Override
    public void run(String... args) throws Exception {
        this.converterService.clearCurrencyDir();
    }
}
