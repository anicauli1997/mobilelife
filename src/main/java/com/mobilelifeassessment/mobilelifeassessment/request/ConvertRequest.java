package com.mobilelifeassessment.mobilelifeassessment.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConvertRequest {

    @NotNull
    private String from;

    @NotNull
    private String to;

    @NotNull
    private Double amount;
}
