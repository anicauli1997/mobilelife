package com.mobilelifeassessment.mobilelifeassessment.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobilelifeassessment.mobilelifeassessment.dto.CurrencyDto;
import com.mobilelifeassessment.mobilelifeassessment.request.ConvertRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class ConverterService {

    @Autowired
    private final CurrencyService currencyService;

    @Autowired
    private final ObjectMapper mapper;

    private final String currencyDirPath = "currencies";

    public Double convert(ConvertRequest convertRequest) throws IOException {
        CurrencyDto currencyDto = this.readCurrenciesFromFile(convertRequest.getFrom().toUpperCase());
        if (!currencyDto.getRates().containsKey(convertRequest.getTo().toUpperCase())) {
            throw new RuntimeException("Currency not available");
        }
        return currencyDto.getRates().get(convertRequest.getTo()) * convertRequest.getAmount();
    }

    private void saveCurrenciesToFile(CurrencyDto currencyDto) throws IOException {
        String fixerApiCurrencyStr = this.mapper.writeValueAsString(currencyDto);

        FileWriter writer = new FileWriter(this.getCurrencyFilePath(currencyDto.getBase()));
        writer.write(fixerApiCurrencyStr);
        writer.close();
    }

    private String getCurrencyFilePath(String baseCurrency) {
        return this.currencyDirPath + "/" + baseCurrency + ".json";
    }

    public void clearCurrencyDir() throws IOException {
        Path directory = Paths.get(this.currencyDirPath);

        Files.list(directory).forEach(file -> {
            try {
                Files.delete(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private CurrencyDto readCurrenciesFromFile(String baseCurrency) throws IOException {
        String currencyFilePath = this.getCurrencyFilePath(baseCurrency);
        File file = new File(currencyFilePath);
        CurrencyDto currencyDto;
        if (!file.exists()) {
            currencyDto = this.currencyService.fetchCurrencies(baseCurrency);
            this.saveCurrenciesToFile(currencyDto);
            return currencyDto;
        }

        currencyDto = this.mapper.readValue(
            new File(this.getCurrencyFilePath(baseCurrency)),
            CurrencyDto.class
        );

        // Check if the currency cache file is updated today, if not, fetch from api and cache the data
        LocalDate latestCurrencyDate = LocalDate.parse(currencyDto.getDate());
        if (latestCurrencyDate.isBefore(LocalDate.now())) {
            currencyDto = this.currencyService.fetchCurrencies(baseCurrency);
            this.saveCurrenciesToFile(currencyDto);
        }
        return currencyDto;
    }
}
