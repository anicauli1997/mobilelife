package com.mobilelifeassessment.mobilelifeassessment.service;

import com.mobilelifeassessment.mobilelifeassessment.dto.CurrencyDto;
import com.mobilelifeassessment.mobilelifeassessment.response.FixerApiCurrencyResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@RequiredArgsConstructor
public class CurrencyService {

    @Value("${fixer-service.url}")
    private String fixerUrl;

    @Value("${fixer-service.apikey}")
    private String fixerApiKey;

    @Autowired
    private final WebClient.Builder webClientBuilder;

    public CurrencyDto fetchCurrencies(String baseCurrency) {
        String uri = fixerUrl + "?access_key=" + fixerApiKey;
        if (fixerUrl.contains("https")) {
            uri = uri + "&base=" + baseCurrency;
        } else if (!baseCurrency.equals("EUR")) {
            throw new RuntimeException("Only EUR is allowed as base currency");
        }
        FixerApiCurrencyResponse fixerApiCurrencyResponse = this.webClientBuilder.build().get().uri(uri)
                .retrieve().bodyToMono(FixerApiCurrencyResponse.class).block();
        CurrencyDto currencyDto = new CurrencyDto();
        assert fixerApiCurrencyResponse != null;
        currencyDto.setBase(fixerApiCurrencyResponse.getBase());
        currencyDto.setDate(fixerApiCurrencyResponse.getDate());
        currencyDto.setRates(fixerApiCurrencyResponse.getRates());
        return currencyDto;
    }
}
