package com.mobilelifeassessment.mobilelifeassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobileLifeAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileLifeAssessmentApplication.class, args);
	}

}
