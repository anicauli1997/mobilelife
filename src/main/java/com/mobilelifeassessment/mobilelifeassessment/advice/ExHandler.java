package com.mobilelifeassessment.mobilelifeassessment.advice;

import com.mobilelifeassessment.mobilelifeassessment.exception.ValidationException;
import com.mobilelifeassessment.mobilelifeassessment.response.ValidationErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExHandler {

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> handleValidationException(ValidationException validationException) {
        return new ResponseEntity<>(
            new ValidationErrorResponse(validationException.getErrors()),
            HttpStatus.UNPROCESSABLE_ENTITY
        );
    }

    @ExceptionHandler(BindException.class)
    protected ResponseEntity<Object> handleBindException(BindException ex) {

        return new ResponseEntity<>(this.getValidationResponse(ex), HttpStatus.UNPROCESSABLE_ENTITY);
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {

        return new ResponseEntity<>(this.getValidationResponse(ex), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    private ValidationErrorResponse getValidationResponse(BindException ex) {

        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            FieldError e = ((FieldError) error);
            String fieldName = e.getField();
            String fieldMessage;
            fieldMessage = e.getDefaultMessage();
            errors.put(fieldName, fieldMessage);
        });

        return new ValidationErrorResponse(errors);
    }

}
