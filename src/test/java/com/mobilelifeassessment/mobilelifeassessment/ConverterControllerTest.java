package com.mobilelifeassessment.mobilelifeassessment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobilelifeassessment.mobilelifeassessment.controller.ConverterController;
import com.mobilelifeassessment.mobilelifeassessment.dto.CurrencyDto;
import com.mobilelifeassessment.mobilelifeassessment.request.ConvertRequest;
import com.mobilelifeassessment.mobilelifeassessment.response.FixerApiCurrencyResponse;
import com.mobilelifeassessment.mobilelifeassessment.service.ConverterService;
import com.mobilelifeassessment.mobilelifeassessment.service.CurrencyService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@AutoConfigureMockMvc
@SpringBootTest
public class ConverterControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private CurrencyService currencyService;

    @Autowired ConverterService converterService;

    @Test
    void testConverted() throws Exception {
        this.converterService.clearCurrencyDir();

        CurrencyDto mockResponse = new CurrencyDto();
        LocalDate date = LocalDate.now();
        String formattedDate = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String baseCurrency = "USD";
        mockResponse.setBase(baseCurrency);
        mockResponse.setDate(formattedDate);
        Map<String, Double> rates = new HashMap<>();
        rates.put("EUR", 0.8);
        mockResponse.setRates(rates);
        Mockito.when(currencyService.fetchCurrencies(baseCurrency)).thenReturn(mockResponse);
        ConvertRequest convertRequest = new ConvertRequest();
        convertRequest.setFrom("USD");
        convertRequest.setTo("EUR");
        convertRequest.setAmount(50.00);
        mockMvc.perform(
                    MockMvcRequestBuilders.post("/api/converter/convert")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(convertRequest))
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("40.0"));

        Mockito.verify(currencyService, Mockito.times(1)).fetchCurrencies(baseCurrency);
    }
}
